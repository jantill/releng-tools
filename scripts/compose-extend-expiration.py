#!/usr/bin/python3

import argparse
import datetime
import logging
from dateutil import parser
from odcs.client.odcs import ODCS, AuthMech, ComposeSourceBuild, ComposeSourceTag, ComposeSourceRawConfig

try:
    import http.client as http_client
except ImportError:
    # Python 2
    import httplib as http_client
http_client.HTTPConnection.debuglevel = 1

# Arguments
aparser = argparse.ArgumentParser()
aparser.add_argument('-c', '--compose', help="compose number", required=True)
aparser.add_argument('-d', '--days', default='3', help="Days, from now, until expiration")
args = aparser.parse_args()
composeNumber = args.compose


# You must initialize logging, otherwise you'll not see debug output.
logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)
requests_log = logging.getLogger("requests.packages.urllib3")
requests_log.setLevel(logging.DEBUG)
requests_log.propagate = True

if args.days:
    days=int(args.days)
else:
    days=4
seconds_added=days * 86400
print("args: " + args.days)
print("days: " + str(days))
print("seconds: "+str(seconds_added))

od = ODCS("https://odcs.stream.rdu2.redhat.com/", auth_mech=AuthMech.Kerberos)
try:
    compose1 = od.renew_compose(composeNumber, seconds_to_live=seconds_added)
    compose2 = od.get_compose(composeNumber)
    print("\nCompose: "+str(compose2['id'])+"  Expires: "+compose2['time_to_expire'])
except Exception as e:
    print(e)


