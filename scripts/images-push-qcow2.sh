#!/usr/bin/bash

COMPOSE="latest-CentOS-Stream"
CENTOS_RELEASE="9-stream"

sync_images() {
    arch=$1
    echo "Start syncing ${arch} images"
    # Stage images so we can make the CHECKSUM
    echo "    Copying to staging"
    mkdir -p stage/images/
    rm -f  stage/images/*
    cp -v \
        /mnt/centos/composes/production/${COMPOSE}/compose/BaseOS/${arch}/images/* \
        stage/images/
    # Generate master CHECKSUM
    rm -f stage/images/MD5SUM stage/images/SHA1SUM
    mv stage/images/SHA256SUM stage/images/SHA256SUM.new
    echo "    Updating CHECKSUM with newest SHA256SUM"
    if wget -o stage/images/CHECKSUM.old https://cloud.centos.org/centos/${CENTOS_RELEASE}/${arch}/images/CHECKSUM ; then
        cat stage/images/SHA256SUM.new stage/images/CHECKSUM.old > stage/images/CHECKSUM
        rm -f stage/images/SHA256SUM.new stage/images/CHECKSUM.old
    else
        mv stage/images/SHA256SUM.new stage/images/CHECKSUM
        rm -f stage/images/CHECKSUM.old
    fi
    # Sync staged images to centos cloud
    echo "    Syncing Everything"
    rsync -avhH --info progress \
        stage/images/ \
        centos@master-1.centos.org:/home/centos-cloud/centos/${CENTOS_RELEASE}/${arch}/images/
    # Cleanup
    rm -rf stage/images/
}

[ -d ~/.ssh ] || mkdir ~/.ssh && chmod 0700 ~/.ssh
if ! grep -q master-1.centos.org ~/.ssh/known_hosts ; then
  ssh-keyscan -t rsa,dsa master-1.centos.org >> ~/.ssh/known_hosts
fi
sync_images aarch64
sync_images ppc64le
sync_images s390x
sync_images x86_64

